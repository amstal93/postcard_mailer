# Generated by Django 2.1.5 on 2019-02-09 17:14

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='postcardcommoncolor',
            name='a',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(255)], verbose_name='Alpha'),
        ),
        migrations.AlterField(
            model_name='postcardcommoncolor',
            name='b',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(255)], verbose_name='Blue'),
        ),
        migrations.AlterField(
            model_name='postcardcommoncolor',
            name='g',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(255)], verbose_name='Green'),
        ),
        migrations.AlterField(
            model_name='postcardcommoncolor',
            name='r',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(255)], verbose_name='Red'),
        ),
    ]
