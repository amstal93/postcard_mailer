from django_filters import rest_framework as filters
from .models import Postcard


class PostcardFilter(filters.FilterSet):
    author = filters.CharFilter(method='filter_author_name')
    style = filters.CharFilter(lookup_expr='exact')

    class Meta:
        model = Postcard
        fields = ['author', 'style']

    def filter_author_name(self, queryset, name, value):
        return queryset.filter(author__username=value)
