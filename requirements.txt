Django==2.1.5
django-templated-mail==1.1.1
djangorestframework==3.9.1
djangorestframework-simplejwt==3.3
djoser==1.4.0
psycopg2==2.7.7
PyJWT==1.7.1
python-dotenv==0.10.1
pytz==2018.9
gunicorn==19.9.0
django-storages==1.7.1
boto3==1.9.91
django-filter
Pillow==5.4.1
sorl-thumbnail==12.5.0
celery==4.2.1
django-rest-swagger==2.2.0
redis==2.10.3
django-redis==4.3.0





